---
layout: job_page
title: "Security Engineer"
---

Our thesis is that [Good Security Is Holistic](https://medium.com/@justin.schuh/stop-buying-bad-security-prescriptions-f18e4f61ba9e).
We think that simulating a security culture in engineering is one of the most
important things. We don't do checklist security, the goal is to keep the trust
of our users by being secure, compliance is not a goal in itself. We don't think
that third party products are unimportant but they are not a silver bullet to
making everything secure.

The [Security Team](/handbook/engineering/security) is responsible for leading and
implementing the various initiatives that relate to improving GitLab's security.

The Security Engineer reports to the [Security Lead](https://about.gitlab.com/jobs/security-lead).

## Responsibilities

* Find and fix security issues within the GitLab code base
* Define, implement, and monitor security measures to protect GitLab.com and company assets
* Respond to reports via GitLab's [bug bounty program](https://medium.com/@collingreene/bug-bounty-5-years-in-c95cda604365#.blaaokpi9)
* Perform vulnerability testing and security assessments
* Provide guidance to the Security Lead and Director of Security regarding new security technologies
* Investigate intrusion incidents, conduct forensic investigations, and mount incident responses
* Collaborate with colleagues on authentication, authorization and encryption solutions

### Specific security-related efforts or projects

- Reduce surface area in application
- Provide guidance to development team regarding proper implementation of security controls and protocols
- Document security trade-offs
- Automated testing/linting
- Offensive (pen testing)
- Detection and response (monitoring, Detection, IDS, OSSEC, updates, response)
- Network security (VPN, VPC, firewalls, access control, also IDS)
- Patch management / Vulnerability management and coordination (modeled after relevant ISO standard)
- Defense in depth recommendations
- Bug bounty program
- Runbooks for incidents, recovery plans
- Abuse (spam, bitcoin mining)
- Package infrastructure/update/release process/patches
- Communication (blog post, postmortems, incident response/crisis communication)
- Dependencies and contribution security risks

## Requirements

- Experience with application and SaaS security in production-level settings (OWASP,
  static/dynamic analysis, common exploit tools and methods)
- Experience with cloud security controls and best practices
- This position requires some development experience and high level of
familiarity with common security libraries, security controls, and common
security flaws that apply to Ruby on Rails applications
- Passion for open source
- Linux experience (e.g. Ubuntu)
- Network security experience (Routing, firewalls, VPNs, common services and protocols)
- Programming experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
- Collaborative team spirit with great communication skills
- You share our [values](/handbook/values), and work in accordance with those values.
